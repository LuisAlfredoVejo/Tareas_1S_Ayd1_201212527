package main

import (
	"fmt"
	"log"
	"net/http"
)

func holaMundoHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola Mundo")
}

func nombreHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Nombre: Luis Alfredo Vejo Mendoza")
}

func carnetHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Carnet: 201212527")
}

func main() {
	http.HandleFunc("/holamundo", holaMundoHandler)
	http.HandleFunc("/nombre", nombreHandler)
	http.HandleFunc("/carnet", carnetHandler)

	port := ":3000"
	fmt.Printf("Servidor API-REST escuchando en http://localhost%s\n", port)
	log.Fatal(http.ListenAndServe(port, nil))
}
