# Tarea 1 Laboratorio Analisis y Diseño 1

## Flujo en Git

![](https://gitlab.com/LuisAlfredoVejo/Tareas_1S_Ayd1_201212527/-/raw/feature/feature-T1/Tarea1/Screenshot_5.jpg?ref_type=heads)

## Servidor Api Rest

![](https://gitlab.com/LuisAlfredoVejo/Tareas_1S_Ayd1_201212527/-/raw/feature/feature-T1/Tarea1/Screenshot_1.jpg?ref_type=heads)

## Endpoint "Hola Mundo"

![](https://gitlab.com/LuisAlfredoVejo/Tareas_1S_Ayd1_201212527/-/raw/feature/feature-T1/Tarea1/Screenshot_2.jpg?ref_type=heads)

## Endpoint "Nombre"

![](https://gitlab.com/LuisAlfredoVejo/Tareas_1S_Ayd1_201212527/-/raw/feature/feature-T1/Tarea1/Screenshot_4.jpg?ref_type=heads)

## Endpoint "Carnet"

![](https://gitlab.com/LuisAlfredoVejo/Tareas_1S_Ayd1_201212527/-/raw/feature/feature-T1/Tarea1/Screenshot_3.jpg?ref_type=heads)